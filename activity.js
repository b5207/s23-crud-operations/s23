db.users.insertMany([
		{
			"firstName": "Harry",
			"lastName": "Styles",
			"email": "harry_styles@gmail.com",
                        "password": "iamharry123",
			"isAdmin": false
		},

		{
			"firstName": "Liam",
			"lastName": "Payne",
			"email": "liam_payne@gmail.com",
                        "password": "iamliam123",
			"isAdmin": false
		},
                
                {
			"firstName": "Zayn",
			"lastName": "Malik",
			"email": "zayn_malik@gmail.com",
                        "password": "iamzayne123",
			"isAdmin": false
		},
                
                {
			"firstName": "Louis",
			"lastName": "Tomlinson",
			"email": "louis_tomlinson@gmail.com",
                        "password": "iamlouis123",
			"isAdmin": false
		},
                
                {
			"firstName": "Niall",
			"lastName": "Horan",
			"email": "niall_horan@gmail.com",
                        "password": "niallhoran123",
			"isAdmin": false
		}

	])
                
                
 db.courses.insertMany([
               {
                   "name": "HTML 101",
                   "price": 2500,
                   "isActive": false
               },
               
               {
                   "name": "CSS 101",
                   "price": 3000,
                   "isActive": false
               },
               
               {
                   "name": "JavaScript 101",
                   "price": 4000,
                   "isActive": false
               }
 ])

//READ
 db.users.find({isAdmin: false})

//UPDATE
 db.users.updateOne({firstName: "Harry"}, {$set: {"isAdmin": true}})

 db.courses.updateOne({name: "JavaScript 101"}, {$set: {"isActive": true}})

//DELETE
 db.courses.deleteMany({isActive: false})